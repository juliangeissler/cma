import glob
import re
import os
import argparse


class Entry:

    def __init__(self, dir_name, file_name, file_title, ref_count):
        self.dir_name = dir_name
        self.file_name = file_name
        self.file_title = file_title
        self.ref_count = ref_count

    @staticmethod
    def get_root():
        return "Cryptographic Migration & Agility"

    def get_dir(self):
        return self.dir_name

    def get_file(self):
        return self.file_name

    def get_title(self):
        return self.file_title

    def get_refs(self):
        return self.ref_count


class MarkdownParser:

    def __init__(self, markdown_dir_path):
        self.markdown_dir_path = markdown_dir_path

    def find_markdown_files(self):
        return glob.glob(self.markdown_dir_path + '/**/*.md', recursive=True)

    def generate_mindmap_file(self):
        md_files = self.find_markdown_files()
        entries = []

        # Iterate files and create entries
        for md_file in md_files:
            dir_name = self.get_md_dir_name(md_file)
            file_name = self.get_md_file_name(md_file)
            file_title = self.get_md_file_title(md_file)
            ref_count = self.get_md_ref_count(md_file)

            if ref_count > 0:
                entries.append(Entry(dir_name, file_name, file_title, ref_count))

        # Write entries to file
        f = open("mindmap_gen/mindmap.md", "w", encoding="utf8")
        f.write(self.generate_mindmap(entries))
        f.close()

    def generate_mindmap(self, entries):
        prev_dir = ""
        mm_str = ""
        mm_str += ("- " + entries[0].get_root() + "\n")
        for entry in entries:
            if entry.get_dir() != "agility" and entry.get_dir() != "migration":
                continue
            if entry.get_dir() == prev_dir:
                if entry.get_dir() == "":
                    mm_str += ("  - " + entry.get_title() + " (Refs: " + str(entry.get_refs()) + ")\n")
                else:
                    mm_str += ("    - " + entry.get_title() + " (Refs: " + str(entry.get_refs()) + ")\n")
            else:
                mm_str += ("  - " + entry.get_dir().capitalize() + "\n")
                mm_str += ("    - " + entry.get_title() + " (Refs: " + str(entry.get_refs()) + ")\n")
            prev_dir = entry.get_dir()

        return mm_str

    def get_md_file_title(self, md_file_path):
        with open(md_file_path, 'r', encoding="utf8") as file:
            data = file.read()
        return re.search("title: \"(.*)\"", data).group(1)

    def get_md_file_name(self, md_file_path):
        file_name = os.path.basename(md_file_path)
        return file_name

    def get_md_dir_name(self, md_file_path):
        dir_name = os.path.basename(os.path.dirname(md_file_path))
        if dir_name == "docs":
            dir_name = ""
        return dir_name

    def get_md_ref_count(self, md_file_path):
        ref_regex = "\.\.\/refs#(.+)\)"
        file_handle = open(md_file_path, mode='r', encoding ="utf8")
        ref_count = 0

        for line in file_handle.readlines():
            match = re.search(ref_regex, line)
            if match is not None:
                ref_count += 1
        return ref_count


parser = argparse.ArgumentParser()
parser.add_argument('-d', '--directory', help='Path to content directory', required=True)
args = parser.parse_args()
content_dir = args.directory

md_parser = MarkdownParser(content_dir)
md_parser.generate_mindmap_file()
