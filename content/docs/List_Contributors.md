---
title: "Contributors"
linktitle: "Contributors"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 10
---
This site was initiated by the research groups [Applied Cyber-Security](https://fbi.h-da.de/forschung/arbeitsgruppen/applied-cyber-security-darmstadt) and [User Centered Security](https://fbi.h-da.de/index.php?id=764) of [Darmstadt University of Applied Sciences](https://h-da.de/), funded by [ATHENE National Research Center for Applied Cybersecurity](https://www.athene-center.de).

A list of our kind contributors:

| Name                           | Affiliation | Contribution |
|--------------------------------|-------------|--------------|
||||
