---
title: "Cryptographic Libraries and Interfaces"
linktitle: "Cryptographic Libraries and Interfaces"
date: 2021-05-06T00:13:06+02:00
draft: false
type: docs
weight: 7
menu:
  main:
    weight: 4
---
- [NaCL (Salt)](https://nacl.cr.yp.to/):
Software library for network communication, encryption, decryption, signatures, etc.

- [Libsodium](https://libsodium.gitbook.io/doc/):
Portable, cross-compilable, installable, packageable fork of NaCl, with a compatible API software library for encryption, decryption, signatures, password hashing etc.

- [LibHydrogen](https://github.com/jedisct1/libhydrogen):
Lightweight crypto library for constrained environments.

- [WASI Cryptography APIs](https://github.com/WebAssembly/wasi-crypto):
Development of cryptography API proposals for the WASI Subgroup of the [WebAssembly Community Group](https://www.w3.org/community/webassembly/)

- [Botan: Crypto and TLS for Modern C++](https://botan.randombit.net/) A  C++ cryptographic library implementing a range of practical systems, such as TLS protocol, X.509 certificates, modern AEAD ciphers, PKCS#11 and TPM hardware support, password hashing, and post quantum crypto schemes. Several other language bindings are available, including Python. Versions of Botan that are approved by the BSI can be found on the [Github repository](https://github.com/Rohde-Schwarz/botan)
