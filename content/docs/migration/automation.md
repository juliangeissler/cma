---
title: "Automation and Frameworks"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 5
---
- RFC6916 PKIs Process Formalization:
  - [Algorithm Agility Procedure for the Resource Public Key Infrastructure (RPKI)](https://tools.ietf.org/html/rfc6916): RFC6916 formalizes the Migration Process for algorithm suites in the Resource Public Key Infrastructure [[GKT13]](../../refs#gkt13)
- Muckle Protocol Security Analysis:
  - [Many a Mickle Makes a Muckle: A Framework for Provably Quantum-Secure Hybrid Key Exchange](https://eprint.iacr.org/2020/099.pdf): Framework for the security analysis of hybrid authenticated key exchange protocols and Introduction of the Muckle protocol [[DHP20]](../../refs#dhp20)
