---
title: "Algorithm Migration Process"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 4
---
- Hybrid TLS & SSH Implementation:
  - [Prototyping post-quantum and hybrid key exchange and authentication in TLS and SSH](https://csrc.nist.gov/CSRC/media/Events/Second-PQC-Standardization-Conference/documents/accepted-papers/stebila-prototyping-post-quantum.pdf): Hybrid approach: Two or more independent algorithms chosen from both post-quantum, and classical schemes [[CPS19]](../../refs#cps19)
  - [Zur Integration von Post-Quantum Verfahren in bestehende Softwarepodukte](https://arxiv.org/pdf/2102.00157v1): Field report on the integration of thePQC methods McEliece and SPHINCS+ based on the eUCRITE API [[ZWH21]](../../refs#zwh21)
- Hybrid Lattice-Based:
  - [ImperialViolet - CECPQ1 results](https://www.imperialviolet.org/2016/11/28/cecpq1.html): Successful experiment using hybrid approach, no network problems and a median connection latency increase of one millisecond [[A.16]](../../refs#a.16)
  - [Experimenting with Post-Quantum Cryptography](https://security.googleblog.com/2016/07/experimenting-with-post-quantum.html): Same experiment, see above [[Bra16]](../../refs#bra16)
  - [Towards post-quantum security for cyber-physical systems: Integrating PQC into industrial m2m communication](http://link.springer.com/10.1007/978-3-030-59013-0_15): Tradeoffs in security: big key/certificate sizes results in problems and difficulties for various protocols.[[PS20]](../../refs#ps20)
  - [Incorporating post-quantum cryptography in a microservice environment](https://homepages.staff.os3.nl/~delaat/rp/2019-2020/p13/report.pdf): Post-Quantum algorithms perform on a similar level to classical ones. The most feasible algorithms are lattice-based. [[WvdG20]](../../refs#wvdg20)
- Hybrid PQ CECPQ2(b) & X25519:
  - [The TLS Post-Quantum Experiment](https://blog.cloudflare.com/the-tls-post-quantum-experiment/): Experiment between google and cloudflare comparing three groups using post-quantum CECPQ2, CECPQ2b or non-post-quantum X25519.[[KV19]](../../refs#kv19)
- Hybrid Certificates:
  - [X.509-Compliant Hybrid Certificates for the Post-Quantum Transition](http://tubiblio.ulb.tu-darmstadt.de/115809/): Parallel usage of two independent cryptographic schemes within public key infrastructures enabling a stepwise transition to post-quantum secureand hybrid algorithms [[BBG+19]](../../refs#bbg19)
- PQC protocol integration:
  <!-- - [Post-Quantum Kryptographie - Classic McEliece](/docs/migration/McEliece.pdf) Introducing a new ASN.1 PQ key format and an evaluation of PQ integration for several cryptographic protocols [[Meun21]](../../refs#meun21) -->
