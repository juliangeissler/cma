---
title: "New Standards"
date: 2021-05-06T00:11:20+02:00
draft: false
type: docs
weight: 6
---
- NIST Report on Round 3 Finalists:
  - [Status report on the second round of the NIST post-quantum cryptography standardization process](https://nvlpubs.nist.gov/nistpubs/ir/2020/NIST.IR.8309.pdf): Third round finalists for public-key encryption / key-establishment algorithms and digital signatures [[MAA+20]](../../refs#maa20)
- Review of NIST Candidates:
  - [Standardisierung von post-quanten-kryptografie und empfehlungen des bsi](https://www.bsi.bund.de/DE/Service-Navi/Veranstaltungen/Deutscher-IT-Sicherheitskongress-30-Jahre-BSI/deutscher-it-sicherheitskongress-30-jahre-bsi_node.html): Overview of the current state of standardization of post Quantum cryptography with respect to the BSI recommendations. [[HKW21]](../../refs#hkw21)
- Open Quantum Project:
  - [Post-quantum Key Exchange for the Internet and the Open Quantum Safe Project](https://eprint.iacr.org/2016/1017.pdf): Open Quantum Project, libqos library: exemplary cryptographic applications like OpenSSL. Comparing NIST Round 2 PQC candidate implementations using OpenSSL [[SM16]](../../refs#sm16)
- DNSSEC PQC Draft:
  - [Retrofitting post-quantum cryptography in internet protocols: a case study of DNSSEC](https://dl.acm.org/doi/10.1145/3431832.3431838): Evaluate three PQC-Algorithms that are suitable for DNSSEC within certain constraints [[MdJvH+20]](../../refs#mdjvh20)
- Decentralized Cert. Management:
  - [Next-generation web public-key infrastructure technologies](https://eprints.qut.edu.au/128643): New decentralized approach to certificate management based on generic blockchains (DPKIT), compatible with existing PKIs. [[HM19]](../../refs#hm19)
