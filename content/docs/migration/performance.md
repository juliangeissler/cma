---
title: "Performance Considerations"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 2
---
Evaluation of the performance of PQC algorithms in various facets, classified into thethree subcategories: *Algorithm Performance, Network Performance, and Hardware Performance*

##### **Algorithm Performance**
- PQC evaluation on chosen hardware:
  - [On Feasibility of Post-Quantum Cryptography on Small Devices](https://www.sciencedirect.com/science/article/pii/S2405896318308474) Experimental post-quantum cryptography implementations on small devices with different platforms [[MPD+18]](../../refs#mpd18)
  - [Towards Practical Deployment of Post-quantum Cryptography on Constrained Platforms and Hardware-Accelerated Platforms](https://link.springer.com/chapter/10.1007/978-3-030-41025-4_8) Evaluation of the NIST candidates regarding their suitability for the implementation on special hardware platforms [[MRD+20]](../../refs#mrd20)
- Improvements to PQC algorithms:
  - [Performance Optimization of Lattice Post-Quantum Cryptographic Algorithms on Many-Core Processors](https://ieeexplore.ieee.org/abstract/document/9238630?casa_token=j7T_SBR8ECgAAAAA:Skx0Ze-JY3YP5CSLn20TOmrWviAP_-aUZ0b9W_gpR5fDpO8AWLigR52JC4qZVPTbLlIzv-3p2g) 52% and 83% improvement in performance for the CRYSTALS-Kyber KEM SHA3 variant and AES variant through Vectorization [[KKP20]](../../refs#kkp20)
  - [Memory-Efficient High-Speed Implementation of Kyber on Cortex-M4](http://link.springer.com/10.1007/978-3-030-23696-0_11) Optimized software implementation of Kyber for the ARM Cortex-M4 microcontroller [[BKS19]](../../refs#bks19)
  - [CTIDH: Faster Constant-Time CSIDH](https://eprint.iacr.org/2021/633.pdf) Speed records for constant-time CSIDH (Commutative Supersingular Isogeny Diffie–Hellman) through combining a new key space with a new algorithm [[BBC+21]](../../refs#bbc21)
- Lattice-based vs. Isogeny-based:
  - [Towards Post-Quantum Security for Cyber-Physical Systems: Integrating PQC into Industrial M2M Communication](https://link.springer.com/chapter/10.1007/978-3-030-59013-0_15) Two solutions for the integration of PQ primitives into the industrial protocol Open Platform Communications Unified Architecture (OPC UA) [[PASC20]](../../refs#pasc20)
  - [Incorporating Post-Quantum Cryptographyin a Microservice Environment](https://homepages.staff.os3.nl/~delaat/rp/2019-2020/p13/report.pdf) On the practical feasibility of using PQCin a microservice architecture [[WvdG20]](../../refs#wvdg20)
- PQC in IoT:
  - [From Pre-Quantum to Post-Quantum IoT Security: A Survey on Quantum-Resistant Cryptosystems for the Internet of Things](https://ieeexplore.ieee.org/abstract/document/8932459) A wide view of post-quantum IoT security and give useful guidelines [[FC20]](../../refs#fc20)

##### **Hardware Performance**
- CRYSTALS-Dilithium and qTesla:
  - [NIST Post-Quantum Cryptography - A Hardware Evaluation Study](https://eprint.iacr.org/2019/047) A hardware-based comparison of the NIST  PQC candidates [[BSNK19]](../../refs#bsnk19)
- Performance critial use cases:
  - [Ultra-Fast Modular Multiplication Implementation for Isogeny-Based Post-Quantum Cryptography](https://ieeexplore.ieee.org/document/9020384) Improved unconventional-radix finite-field multiplication (IFFM) algorithm reducing computational complexity by about 20% [[TLW19]](../../refs#tlw19)
- FPGA performance benefits:
  - [Implementation and benchmarking of round 2 candidates in the NIST post-quantum cryptography standardization process using hardware and software/hardware co-design approaches](https://cryptography.gmu.edu/athena/PQC/GMU_PQC_2020_SW_HW.pdf) Methodology for implementing and benchmarking PQC candidates usingboth hardware and software/hardware co-design approaches [[DFA+20]](../../refs#dfa20)
  - [Post-Quantum Cryptography on FPGA Based on Isogenies on Elliptic Curves](https://ieeexplore.ieee.org/abstract/document/7725935) Isogeny-based schemes can be implemented with high efficiency on reconfigurable hardware [[KAMJ17]](../../refs#kamj17)
  - [Post-Quantum Secure Boot](https://ieeexplore.ieee.org/document/9116252) Post-quantum secure boot solution implemented fully as hardware for reasons of security and performance [[KGC+20]](../../refs#kgc20)
- Hardware Security Modules (HSMs):
  - [Post-Quantum Secure Architectures for Automotive Hardware Secure Modules](https://eprint.iacr.org/2020/026.pdf) Building a post-quantum secure automotive HSM is feasible and can meet the hard requirements imposed by a modern vehicle ECU [[WaSt20]](../../refs#wast20)

##### **Network Performacne**
- Measurments and benchmarks:
  - [Benchmarking Post-Quantum Cryptography in TLS](https://eprint.iacr.org/2019/1447) Packet loss rates above 3–5% start to have a significantimpact on post-quantum algorithms that fragment across many packets [[PST19]](../../refs#pst19)
  - [Real-world measurements of structured-lattices and supersingular isogenies in TLS](https://www.imperialviolet.org/2019/10/30/pqsivssl.html) Computational advantages of structured lattices make them a more attractive choice for post-quantum confidentiality [[Lang19]](../../refs#lang19)
  - [Measuring TLS key exchange with post-quantum KEM](https://csrc.nist.gov/CSRC/media/Events/Second-PQC-Standardization-Conference/documents/accepted-papers/kwiatkowski-measuring-tls.pdf) [[KSL+19]](../../refs#ksl19)
  - [Post-Quantum Authentication in TLS 1.3: A Performance Study](http://eprint.iacr.org/2020/071) Detailed performance evaluation of the NIST signature algorithm candidates and imposed latency on TLS 1.3 [[SKD20]](../../refs#skd20)
- TLS, DTLS, IKEv2 and QUIC PQC integrations:
  - [The TLS Post-Quantum Experiment](https://blog.cloudflare.com/the-tls-post-quantum-experiment/) Evaluating the performance and feasibility of deployment in TLS of two post-quantum key agreement ciphers [[KwVa19]](../../refs#kwva19)
  - [Post-Quantum TLS on Embedded Systems: Integrating and Evaluating Kyberand SPHINCS+ with Mbed TLS](https://dl.acm.org/doi/abs/10.1145/3320269.3384725) Post-quantum key establishment with Kyber performs well in TLS on embedded devices compared to ECC variants [[BSKNS20]](../../refs#bskns20)
  - [The Viability of Post-quantum X.509 Certificates](https://eprint.iacr.org/2018/063) Signature schemes standardized in NIST PQ Project can work with X.509certs in a post-quantum Internet [[KPDG18]](../../refs#kpdg18)
  - [Post-quantum Key Exchange for the Internet and the Open Quantum Safe Project](https://link.springer.com/chapter/10.1007%2F978-3-319-69453-5_2)  [[StMo16]](../../refs#stmo16)
- VPN evaluations:
  - [Two PQ Signature Use-cases: Non-issues, challenges and potential solutions](https://eprint.iacr.org/2019/1276)  Dilithium and Falcon are the best available options but come with an impact on TLS performance [[KaSi19]](../../refs#kasi19)
