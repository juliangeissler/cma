---
title: "Development Considerations"
linktitle: "Development Considerations"
date: 2021-05-06T00:12:02+02:00
draft: false
type: docs
weight: 2
---
- Research on CA mechanism
  - [On the importance of cryptographic agility for industrial automation](https://www.degruyter.com/document/doi/10.1515/auto-2019-0019/html) This work motivates cryptographic agility by discussing the threat of quantum computers to modern cryptography [[PN19]](../../refs#pn19)
  - [Security issues on the CNG cryptography library (Cryptography API: Next Generation)](https://ieeexplore.ieee.org/document/6603762) Next Generation from Microsoft to exchange cryptographic algorithms without any change to the code of the program [[LLP+13]](../../refs#llp13)
  - [API Usability of Stateful Signature Schemes](https://link.springer.com/chapter/10.1007/978-3-030-26834-3_13) Easy-to-use API design for stateful signature schemes [[ZWH19]](../../refs#zwh19)
- CA as design principle
  - [PQFabric: A Permissioned Blockchain Secure from Both Classical and Quantum Attacks](https://arxiv.org/abs/2010.06571) Proposes a redesign of Fabric's credential-management procedures and related specifications in order to incorporate hybrid digital signatures, protecting against both classical and quantum attacks using one classical and one quantum-safe signature [[HPDM20]](../../refs#hpdm20)
  - [Public Key Infrastructure and Crypto Agility Concept for Intelligent Transportation Systems](http://www.thinkmind.org/index.php?view=article&articleid=vehicular_2015_1_30_30028) This paper proposes a multi-domain PKI architecture for intelligent transportation systems, which considers the necessities of road infrastructure authorities and vehicle manufacturers, today [[UWK15]](../../refs#uwk15)
- Eval crypto libs
  - [Comparing the Usability of Cryptographic APIs](https://ieeexplore.ieee.org/document/7958576) This paper is the first to examine both how and why the design and resulting usability of different cryptographic libraries affects the security of code written with them [[ABF+17]](../../refs#abf17)
- Eval code examples for crypto libs
  - [Usability and Security Effects of Code Examples on Crypto APIs](https://ieeexplore.ieee.org/document/8514203) Platform for cryptographic code examples that improves the usability and security of created applications by non security experts [[MW18]](../../refs#mw18)
  - [Fluid Intelligence Doesn't Matter! Effects of Code Examples on the Usability of Crypto APIs](https://arxiv.org/abs/2004.03973) Researches whether similarity and Gf also have an effect in the context of using cryptographic APIs [[MW20]](../../refs#mw20)
- Eval docum. system for crypto libs
  - [Zur Benutzbarkeit und Verwendung von API-Dokumentationen](https://dl.gi.de/handle/20.500.12116/33515) Showcases requirements for a good security API [[HZHW20]](../../refs#hzhw20)
