---
title: "Incentives"
linktitle: "Incentives"
date: 2021-05-06T00:12:16+02:00
draft: false
type: docs
weight: 4
---
- Ranking by best practice as incentive
  - [Biggest Failures in Security](https://drops.dagstuhl.de/opus/volltexte/2020/11981/) Tries to identify the "biggest failures" in security and to get a comprehensive understanding on their overall impact on security. [[AVVY19]](../../refs#avvy19)
- Best practice for agility in protocols
  - [Guidelines for Cryptographic Algorithm Agility and Selecting Mandatory-to-Implement Algorithms](https://tools.ietf.org/html/rfc7696) Provides guidelines to ensure that protocols have the ability to migrate from one mandatory-to-implement algorithm suite to another over time. [[Hou15]](../../refs#hou15)
- Building blocks of crypto-agility
  - [On the importance of cryptographic agility for industrial automation](https://www.degruyter.com/document/doi/10.1515/auto-2019-0019/html) This work motivates cryptographic agility by discussing the threat of quantum computers to moderncryptography. [[PN19]](../../refs#pn19)

