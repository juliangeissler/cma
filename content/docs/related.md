---
title: "Related Work"
linktitle: "Related Work"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 3
---
A collection of survey papers and references dealing with general challenges and recommendations regarding the migration to post-quantum cryptography and cryptographic agility.

*A full reference list can  be found in the [references](../refs) section. All references are listed in alphabetical order.*

- [Identifying Research Challenges in Post Quantum Cryptography Migration and Cryptographic Agility](http://arxiv.org/abs/1909.07353): A wide range of topics and challenges at a high abstraction level grouped into categories of PQC migration and crypto-agility [[OPp19]](../refs#opp19)
- [Getting Ready for Post-Quantum Cryptography](https://nvlpubs.nist.gov/nistpubs/CSWP/NIST.CSWP.04282021.pdf): Challenges Associated with Adopting and Using Post-Quantum Cryptographic Algorithms [[BPS21]](../refs#bps21).
- [Practical Post-Quantum Cryptography](https://www.sit.fraunhofer.de/fileadmin/dokumente/studien_und_technical_reports/Practical.PostQuantum.Cryptography_WP_FraunhoferSIT.pdf?_=1503992279): White paper from the Fraunhofer Institute for Secure Information Technology SIT addressing challenges of PQC migration and comparison of PQC algorithms [[NIWA17]](../refs#niwa17).
- [From Pre-Quantum to Post-Quantum IoT Security](https://ieeexplore.ieee.org/document/8932459): Challenges for PQC in IoT and comparison of the performance of PQC algorithms [[FC20]](../refs#fc20).
- [Biggest Failures in IT Security](https://drops.dagstuhl.de/opus/volltexte/2020/11981/pdf/dagrep_v009_i011_p001_19451.pdf): A variety of problems in achieving IT security and possible strategies to solve them [[AVVY19]](../refs#avvy19).
- [Getting Ready for Post-Quantum Cryptography](https://nvlpubs.nist.gov/nistpubs/CSWP/NIST.CSWP.05262020-draft.pdf): Challenges associated with adoption and use of post-quantum cryptographic algorithms [[BPS20]](../refs#bps20).
- [Migration zu Post-Quanten-Kryptografie](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Krypto/Post-Quanten-Kryptografie.html): Recommendations for action on migration to PQC by the BSI (German Federal Office for Information Security) [[BSI20]](../refs#bsi20).
- [Quantencomputerresistente Kryptografie: Aktuelle Aktivitäten und Fragestellungen](https://www.secumedia-shop.net/Deutschland-Digital-Sicher-30-Jahre-BSI): A brief evaluation of the current state of both post-quantum and quantum cryptography [[HLL+21]](../refs#hll21).
- [Quantum Safe Cryptography and Security: An introduction, benefits, enablers and challenges](https://www.etsi.org/images/files/ETSIWhitePapers/QuantumSafeWhitepaper.pdf): Important use cases for cryptography and potential migration strategies to transition to post-quantum cryptography [[CCD+15]](../refs#ccd15).
